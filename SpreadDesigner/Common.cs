﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpreadDesigner
{
  using FarPoint.Localization;
  using FarPoint.Win.Spread;
  using FarPoint.Win.Spread.Chart;
  using Microsoft.Win32;
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.ComponentModel.Design;
  using System.Diagnostics;
  using System.Drawing;
  using System.Globalization;
  using System.Reflection;
  using System.Resources;
  using System.Runtime.CompilerServices;
  using System.Text;
  using System.Threading;
  using System.Windows.Forms;

  public sealed class common
  {
    public static ResourceSet rm = null;

    public static void DisplayHelp(object owner, string topic)
    {
      string str4 = @"\Docs\Windows Forms\";
      RegistryKey key = Registry.ClassesRoot.OpenSubKey(@"FarPoint.Win.Spread\\v8.20.20143.0\\InstallDir");
      string str2 = (key == null ? string.Empty : key.GetValue(null)).ToString();
      RegistryKey key2 = Registry.LocalMachine.OpenSubKey(@"Software\\Microsoft\\Windows\\HTML Help");
      RegistryKey key3 = Registry.LocalMachine.OpenSubKey(@"Software\\Wow6432Node\\Microsoft\\Windows\\HTML Help");
      string str3 = string.Empty;
      string name = "FarPoint.Win.Spread.7.0.chm";
      if ((key2 == null) & (key3 == null))
      {
        str3 = str2 + @"\Docs\Windows Forms\";
      }
      else if (key2 != null)
      {
        str3 = key2.GetValue(name, "Empty").ToString();
        if (str3 == "Empty")
        {
          if (key3 != null)
          {
            str3 = key3.GetValue(name, str2 + str4).ToString();
          }
          else
          {
            str3 = str2 + str4;
          }
        }
      }
      else if (key3 != null)
      {
        str3 = key3.GetValue(name, str2 + str4).ToString();
      }
      Help.ShowHelp((Control)owner, str3 + name, topic);
    }

    public static string GetAssemblyVersion(object obj)
    {
      string str3 = string.Empty;
      if (obj != null)
      {
        string fullName = obj.GetType().Assembly.FullName;
        int index = fullName.IndexOf("Version=");
        if (index > 0)
        {
          int num2 = fullName.IndexOf(",", (int)(index + 8));
          if (num2 > 0)
          {
            str3 = fullName.Substring(index + 8, (num2 - index) - 8).Trim();
          }
        }
      }
      return str3;
    }

    public static System.Drawing.Color GetInvertedColor(System.Drawing.Color c)
    {
      if (((c.R + c.G) + c.B) > 1)
      {
        return System.Drawing.Color.Black;
      }
      return System.Drawing.Color.White;
    }

    internal static bool IsContextMenuUsedByMultipleSpread(IDesignerHost designerHost, ContextMenuStrip contextMenu)
    {
      IEnumerator enumerator = null;
      if (designerHost == null)
      {
        return true;
      }
      if (contextMenu == null)
      {
        return true;
      }
      int num = 0;
      bool flag = false;
      try
      {
        enumerator = designerHost.Container.Components.GetEnumerator();
        while (enumerator.MoveNext())
        {
          IComponent current = (IComponent)enumerator.Current;
          if (current is FpSpread)
          {
            IEnumerator enumerator2 = null;
            flag = false;
            try
            {
              enumerator2 = ((FpSpread)current).Sheets.GetEnumerator();
              while (enumerator2.MoveNext())
              {
                IEnumerator enumerator3 = null;
                SheetView view = (SheetView)enumerator2.Current;
                try
                {
                  enumerator3 = view.Charts.GetEnumerator();
                  while (enumerator3.MoveNext())
                  {
                    SpreadChart chart = (SpreadChart)enumerator3.Current;
                    if ((chart.ContextMenuStrip != null) && chart.ContextMenuStrip.Equals(contextMenu))
                    {
                      flag = true;
                      continue;
                    }
                  }
                }
                finally
                {
                  if (enumerator3 is IDisposable)
                  {
                    (enumerator3 as IDisposable).Dispose();
                  }
                }
              }
            }
            finally
            {
              if (enumerator2 is IDisposable)
              {
                (enumerator2 as IDisposable).Dispose();
              }
            }
            if (flag)
            {
              num++;
            }
          }
        }
      }
      finally
      {
        if (enumerator is IDisposable)
        {
          (enumerator as IDisposable).Dispose();
        }
      }
      return (num > 1);
    }

    public static void LoadResourceManagerForStrings()
    {
      string name = Thread.CurrentThread.CurrentUICulture.Name;
      LocalizationResourceManager manager = null;
      manager = new LocalizationResourceManager("SpreadWinDesigner", Assembly.GetAssembly(typeof(SpreadDesigner)));
      if (manager == null)
      {
        MessageBox.Show("Unable to load resources.", "Resource load failure");
      }
      else
      {
        if (name == "en-US")
        {
          rm = manager.GetResourceSet(new CultureInfo(""), true, true);
        }
        else
        {
          rm = manager.GetResourceSet(Thread.CurrentThread.CurrentUICulture, true, true);
        }
        if (rm == null)
        {
          rm = manager.GetResourceSet(new CultureInfo(""), true, true);
        }
      }
    }

    internal static void RemoveChartContextMenuFromDesignerHost(FpSpread spread)
    {
      if ((spread != null) && (spread.Site != null))
      {
        IDesignerHost service = (IDesignerHost)spread.Site.GetService(typeof(IDesignerHost));
        if (service != null)
        {
          int num3 = spread.Sheets.Count - 1;
          for (int i = 0; i <= num3; i++)
          {
            int num4 = spread.Sheets[i].Charts.Count - 1;
            for (int j = 0; j <= num4; j++)
            {
              if ((spread.Sheets[i].Charts[j].ContextMenuStrip != null) && !IsContextMenuUsedByMultipleSpread(service, spread.Sheets[i].Charts[j].ContextMenuStrip))
              {
                service.Container.Remove(spread.Sheets[i].Charts[j].ContextMenuStrip);
              }
            }
          }
        }
      }
    }

    internal static void RestoreChartContextMenu(FpSpread spread, Dictionary<string, ContextMenuStrip> chartContextMenus, Dictionary<string, string> chartContextMenuNames)
    {
      if (((spread != null) && (chartContextMenus != null)) && ((chartContextMenuNames != null) && (spread.Site != null)))
      {
        IDesignerHost service = (IDesignerHost)spread.Site.GetService(typeof(IDesignerHost));
        if (service != null)
        {
          string str = null;
          int num3 = spread.Sheets.Count - 1;
          for (int i = 0; i <= num3; i++)
          {
            SheetView view = spread.Sheets[i];
            int num4 = view.Charts.Count - 1;
            for (int j = 0; j <= num4; j++)
            {
              SpreadChart chart = view.Charts[j];
              if (chartContextMenus.ContainsKey(view.SheetName + "_" + chart.ChartName))
              {
                chart.ContextMenuStrip = chartContextMenus[view.SheetName + "_" + chart.ChartName];
              }
              if (chartContextMenuNames.ContainsKey(view.SheetName + "_" + chart.ChartName))
              {
                str = chartContextMenuNames[view.SheetName + "_" + chart.ChartName];
              }
              if (chart.ContextMenuStrip != null)
              {
                if (string.IsNullOrEmpty(str))
                {
                  service.Container.Add(chart.ContextMenuStrip);
                }
                else
                {
                  service.Container.Add(chart.ContextMenuStrip, str);
                }
              }
            }
          }
        }
      }
    }

    internal static void SaveChartContextMenu(FpSpread spread, ref Dictionary<string, ContextMenuStrip> chartContextMenus, ref Dictionary<string, string> chartContextMenuNames)
    {
      if (spread != null)
      {
        IEnumerator enumerator = null;
        if (chartContextMenus == null)
        {
          chartContextMenus = new Dictionary<string, ContextMenuStrip>();
        }
        if (chartContextMenuNames == null)
        {
          chartContextMenuNames = new Dictionary<string, string>();
        }
        try
        {
          enumerator = spread.Sheets.GetEnumerator();
          while (enumerator.MoveNext())
          {
            SheetView current = (SheetView)enumerator.Current;
            if (current.Charts.Count > 0)
            {
              IEnumerator enumerator2 = null;
              try
              {
                enumerator2 = current.Charts.GetEnumerator();
                while (enumerator2.MoveNext())
                {
                  SpreadChart chart = (SpreadChart)enumerator2.Current;
                  if (chart.ContextMenuStrip != null)
                  {
                    chartContextMenus.Add(current.SheetName + "_" + chart.ChartName, chart.ContextMenuStrip);
                    chartContextMenuNames.Add(current.SheetName + "_" + chart.ChartName, chart.ContextMenuStrip.Name);
                  }
                }
              }
              finally
              {
                if (enumerator2 is IDisposable)
                {
                  (enumerator2 as IDisposable).Dispose();
                }
              }
            }
          }
        }
        finally
        {
          if (enumerator is IDisposable)
          {
            (enumerator as IDisposable).Dispose();
          }
        }
      }
    }

    public static void PaintSeparator(object sender, System.Windows.Forms.PaintEventArgs e)
    {
      Control control = sender as Control;
      // Create the pens to draw the line
      System.Drawing.Pen penDark = new System.Drawing.Pen(System.Drawing.SystemColors.ControlDark);
      System.Drawing.Pen penWhite = new System.Drawing.Pen(System.Drawing.Color.White);
      int x = 0;
      int y = 0;

      // Measure the text in the Label control
      // bug 99908597 2009/06/04 -sun >>

      System.Drawing.StringFormat stringFormat = new System.Drawing.StringFormat();
      stringFormat.FormatFlags = System.Drawing.StringFormatFlags.MeasureTrailingSpaces;
      System.Drawing.SizeF size = e.Graphics.MeasureString(control.Text, control.Font, e.ClipRectangle.Width, stringFormat);

      // << bug 99908597
      // Offset the x so the line does not touch the text

      x = (int)size.Width + 13;

      x = (int)size.Width + 13;
      // Center the line vertically in the control
      y = (int)size.Height / 2;

      // Draw the line, dark above and highlight below
      e.Graphics.DrawLine(penDark, x, y, control.Width, y);
      e.Graphics.DrawLine(penWhite, x, y + 1, control.Width, y + 1);

      // Dispose of the pens we created
      penDark.Dispose();
      penWhite.Dispose();
    }

    private class HiddenComponentConverter : ComponentConverter
    {
      public HiddenComponentConverter(System.Type type)
        : base(type)
      {
      }

      public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
      {
        return false;
      }
    }

    [Browsable(false), ToolboxItem(false), TypeConverter(typeof(common.HiddenComponentConverter))]
    public class HyperLink : Component
    {
      private string _link;
      private System.Windows.Forms.LinkArea _linkArea;
      private string _linkToolTip;

      [DebuggerNonUserCode]
      public HyperLink()
      {
      }

      public string Link
      {
        get
        {
          return this._link;
        }
        set
        {
          this._link = value;
        }
      }

      public System.Windows.Forms.LinkArea LinkArea
      {
        get
        {
          return this._linkArea;
        }
        set
        {
          this._linkArea = value;
        }
      }

      public string LinkToolTip
      {
        get
        {
          return this._linkToolTip;
        }
        set
        {
          this._linkToolTip = value;
        }
      }
    }

    [Category("HyperLinkEditor"), Browsable(false)]
    public class HyperlinkEditor
    {
      private common.HyperLink[] _links = new common.HyperLink[0];

      [Category("HyperLinks")]
      public common.HyperLink[] Links
      {
        get
        {
          return this._links;
        }
        set
        {
          this._links = value;
        }
      }
    }
  }
}
