﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
namespace SpreadDesigner
{
  internal class CheckedListComboBox : System.Windows.Forms.ComboBox
  {
    private string displayTextField;
    private double dpiScale;
    private ListBoxNative dropDown;

    public event ItemCheckEventHandler CheckStateChanged;

    public CheckedListComboBox()
    {
      this.dpiScale = 1.0;
      base.DrawMode = DrawMode.OwnerDrawFixed;
      base.DropDownStyle = ComboBoxStyle.DropDownList;
      base.DoubleBuffered = true;
      base.Items.Add(new CheckedListComboBoxItem(common.rm.GetString("AllItemComboBox"), false));
    }
    
    protected override void OnDrawItem(DrawItemEventArgs e)
    {
      PointF tf;
      if (!(this.dpiScale == 1.0))
      {
        this.SuspendLayout();
        this.ItemHeight = (int)Math.Round((double)(this.ItemHeight * this.dpiScale));
        this.ResumeLayout(false);
      }
      Font font = new Font(this.Font.FontFamily, (float)(this.Font.Size * this.dpiScale), this.Font.Style, this.Font.Unit);
      Size glyphSize = CheckBoxRenderer.GetGlyphSize(e.Graphics, CheckBoxState.UncheckedNormal);
      int num = (int)Math.Round((double)(((double)(this.ItemHeight - glyphSize.Height)) / 2.0));
      int num2 = num - 1;
      if (e.Index == -1)
      {
        SolidBrush brush4 = new SolidBrush(SystemColors.WindowText);
        tf = new PointF((float)e.Bounds.X, (float)(e.Bounds.Y + num2));
        e.Graphics.DrawString(this.displayTextField, font, brush4, tf);
        brush4.Dispose();
        if (this.Focused)
        {
          e.DrawFocusRectangle();
        }
      }
      else if (!this.DroppedDown | (e.State == DrawItemState.ComboBoxEdit))
      {
        SolidBrush brush = new SolidBrush(SystemColors.WindowText);
        tf = new PointF((float)e.Bounds.X, (float)(e.Bounds.Y + num2));
        e.Graphics.DrawString(this.displayTextField, font, brush, tf);
        brush.Dispose();
        if (this.Focused)
        {
          e.DrawFocusRectangle();
        }
      }
      else
      {
        e.DrawBackground();
        if (!(base.Items[e.Index] is CheckedListComboBoxItem))
        {
          SolidBrush brush2 = new SolidBrush(SystemColors.WindowText);
          tf = new PointF((float)e.Bounds.X, (float)(e.Bounds.Y + num2));
          e.Graphics.DrawString(base.Items[e.Index].ToString(), font, brush2, tf);
          brush2.Dispose();
        }
        else
        {
          SolidBrush brush3;
          CheckBoxState uncheckedNormal = CheckBoxState.UncheckedNormal;
          CheckedListComboBoxItem item = (CheckedListComboBoxItem)base.Items[e.Index];
          CheckBoxRenderer.RenderMatchingApplicationState = true;
          switch (item.CheckState)
          {
            case CheckState.Unchecked:
              uncheckedNormal = CheckBoxState.UncheckedNormal;
              break;

            case CheckState.Checked:
              uncheckedNormal = CheckBoxState.CheckedNormal;
              break;

            case CheckState.Indeterminate:
              uncheckedNormal = CheckBoxState.MixedNormal;
              break;
          }
          Point glyphLocation = new Point(e.Bounds.X + num, e.Bounds.Y + num);
          CheckBoxRenderer.DrawCheckBox(e.Graphics, glyphLocation, uncheckedNormal);
          if ((e.State & DrawItemState.Focus) == DrawItemState.Focus)
          {
            brush3 = new SolidBrush(SystemColors.HighlightText);
          }
          else
          {
            brush3 = new SolidBrush(SystemColors.WindowText);
          }
          tf = new PointF((float)((e.Bounds.X + num) + e.Bounds.Height), (float)(e.Bounds.Y + num));
          e.Graphics.DrawString(item.Text, font, brush3, tf);
          brush3.Dispose();
        }
      }
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      Keys keyCode = e.KeyCode;
      if (keyCode == Keys.Space)
      {
        if (this.DroppedDown && (this.SelectedItem is CheckedListComboBoxItem))
        {
          this.dropDown.CheckItem(this.SelectedIndex);
        }
      }
      else if ((keyCode == Keys.Down) && !this.DroppedDown)
      {
        this.DroppedDown = true;
        this.SelectedIndex = -1;
        return;
      }
      base.OnKeyDown(e);
    }

    internal void RaiseCheckStateChanged(object sender, ItemCheckEventArgs e)
    {
      ItemCheckEventHandler checkStateChangedEvent = this.CheckStateChanged;
      if (checkStateChangedEvent != null)
      {
        checkStateChangedEvent(RuntimeHelpers.GetObjectValue(sender), e);
      }
    }

    protected override void ScaleControl(SizeF factor, BoundsSpecified specified)
    {
      this.dpiScale = factor.Width;
      base.ScaleControl(factor, specified);
    }

    public static void Sort(List<CheckedListComboBoxItem> array)
    {
      int num = array.Count - 1;
      int num4 = num;
      for (int i = 0; i <= num4; i++)
      {
        int num5 = num;
        for (int j = i + 1; j <= num5; j++)
        {
          if (string.Compare(array[i].Text, array[j].Text, false) > 0)
          {
            object obj2 = array[i];
            array[i] = array[j];
            array[j] = (CheckedListComboBoxItem)obj2;
          }
        }
      }
    }

    protected override void WndProc(ref Message m)
    {
      base.WndProc(ref m);
      if (m.Msg == 0x210)
      {
        long num = ((long)m.WParam) & 0xffffL;
        if (num == 1L)
        {
          if (this.dropDown == null)
          {
            this.dropDown = new ListBoxNative(this);
          }
          this.dropDown.AssignHandle(m.LParam);
        }
      }
    }

    public string DisplayText
    {
      get
      {
        return this.displayTextField;
      }
      set
      {
        this.displayTextField = value;
        this.Invalidate();
      }
    }
  }
  
  internal class CheckedListComboBoxItem
  {
    private System.Windows.Forms.CheckState _checkState = System.Windows.Forms.CheckState.Unchecked;
    private object _tag = null;
    private string _text = "";

    public CheckedListComboBoxItem(string text, bool initialCheckState)
    {
      this._checkState = initialCheckState ? System.Windows.Forms.CheckState.Checked : System.Windows.Forms.CheckState.Unchecked;
      this._text = text;
    }

    public System.Windows.Forms.CheckState CheckState
    {
      get
      {
        return this._checkState;
      }
      set
      {
        if (value != this._checkState)
        {
          this._checkState = value;
        }
      }
    }

    public object Tag
    {
      get
      {
        return this._tag;
      }
      set
      {
        this._tag = RuntimeHelpers.GetObjectValue(value);
      }
    }

    public string Text
    {
      get
      {
        return this._text;
      }
      set
      {
        this._text = value;
      }
    }
  }

}

